package com.yalantis.videoeffectsdemo.view.shader

/**
 * Created by irinagalata on 3/10/17.
 */
object BlackAndWhiteShader : Shader {

    // language=GLSL
    override val fragmentShader = """
        #extension GL_OES_EGL_image_external : require
        precision mediump float;
        varying vec2 v_UV;
        uniform samplerExternalOES u_Text;
        void main() {
            vec4 color = texture2D(u_Text, v_UV);
            float colorR = (color.r + color.g + color.b) / 3.0;
            float colorG = (color.r + color.g + color.b) / 3.0;
            float colorB = (color.r + color.g + color.b) / 3.0;
            gl_FragColor = vec4(colorR, colorG, colorB, color.a);
        }
    """

}