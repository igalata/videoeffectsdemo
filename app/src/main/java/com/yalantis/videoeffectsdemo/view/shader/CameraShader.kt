package com.yalantis.videoeffectsdemo.view.shader

/**
 * Created by irinagalata on 3/6/17.
 */
object CameraShader {

    const val A_POSITION = "a_Position"
    const val A_UV = "a_UV"

    const val U_TEXT = "u_Text"
    const val U_EFFECT_APPLIED = "u_EffectApplied"

    // language=GLSL
    val vertexShader = """
        attribute vec4 a_Position;
        attribute vec2 a_UV;

        varying vec2 v_UV;

        void main()
        {
            gl_Position = a_Position;
            v_UV = a_UV;
        }
    """

    // language=GLSL
    val fragmentShader = """
        #extension GL_OES_EGL_image_external : require
        precision mediump float;

        uniform samplerExternalOES u_Text;
        uniform float u_EffectApplied;

        varying vec2 v_UV;

        void main()
        {
            gl_FragColor = texture2D(u_Text, v_UV);
        }
    """

}