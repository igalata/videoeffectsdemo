package com.yalantis.videoeffectsdemo.view.shader

/**
 * Created by irinagalata on 3/10/17.
 */
object SepiaShader : Shader {

    // language=GLSL
    override val fragmentShader = """
        #extension GL_OES_EGL_image_external : require
        precision mediump float;
        uniform samplerExternalOES u_Text;
        varying vec2 v_UV;
        void main() {
            mat3 matrix = mat3(805.0f / 2048.0f, 715.0f / 2048.0f,
                557.0f / 2048.0f, 1575.0f / 2048.0f, 1405.0f / 2048.0f,
                1097.0f / 2048.0f, 387.0f / 2048.0f, 344.0f / 2048.0f,
                268.0f / 2048.0f);
            vec4 color = texture2D(u_Text, v_UV);
            vec3 new_color = min(matrix * color.rgb, 1.0);
            gl_FragColor = vec4(new_color.rgb, color.a);
       }
    """

}