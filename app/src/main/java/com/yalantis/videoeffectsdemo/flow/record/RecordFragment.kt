package com.yalantis.videoeffectsdemo.flow.record

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yalantis.videoeffectsdemo.R
import com.yalantis.videoeffectsdemo.view.Effect
import kotlinx.android.synthetic.main.fragment_record.*

/**
 * Created by irinagalata on 3/7/17.
 */
class RecordFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_record, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonBlack.setOnClickListener { cameraView.effect = Effect.BLACK_AND_WHITE }
        buttonSepia.setOnClickListener { cameraView.effect = Effect.SEPIA }
        buttonNone.setOnClickListener { cameraView.effect = Effect.NONE }
    }

    override fun onResume() {
        super.onResume()
        cameraView.onResume()
    }

    override fun onPause() {
        super.onPause()
        cameraView.onPause()
    }

}