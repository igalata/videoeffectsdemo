package com.yalantis.videoeffectsdemo.view

/**
 * Created by irinagalata on 3/10/17.
 */
enum class Effect {

    SEPIA, BLACK_AND_WHITE, NONE

}