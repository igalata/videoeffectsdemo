package com.yalantis.videoeffectsdemo.view

import android.annotation.SuppressLint
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.opengl.GLES11Ext.GL_TEXTURE_EXTERNAL_OES
import android.opengl.GLES20.*
import android.opengl.GLSurfaceView
import com.yalantis.videoeffectsdemo.ShaderHelper
import com.yalantis.videoeffectsdemo.view.shader.BlackAndWhiteShader
import com.yalantis.videoeffectsdemo.view.shader.CameraShader
import com.yalantis.videoeffectsdemo.view.shader.CameraShader.A_POSITION
import com.yalantis.videoeffectsdemo.view.shader.CameraShader.A_UV
import com.yalantis.videoeffectsdemo.view.shader.CameraShader.U_EFFECT_APPLIED
import com.yalantis.videoeffectsdemo.view.shader.CameraShader.U_TEXT
import com.yalantis.videoeffectsdemo.view.shader.CameraShader.vertexShader
import com.yalantis.videoeffectsdemo.view.shader.SepiaShader
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


/**
 * Created by irinagalata on 3/6/17.
 */
class CameraRenderer : GLSurfaceView.Renderer {

    var view: CameraView? = null
    var effect: Effect = Effect.NONE

    private val vertices = floatArrayOf(
            -1f, -1f,
            1f, -1f,
            -1f, 1f,
            1f, 1f
    )

    private val textureVertices = floatArrayOf(
            0f, 1f,
            1f, 1f,
            0f, 0f,
            1f, 0f
    )

    private var verticesBuffer: FloatBuffer? = null
    private var textureBuffer: FloatBuffer? = null
    private var programId = 0
    private var sepiaProgramId = 0
    private var blackAndWhiteProgramId = 0
    private var camera: Camera? = null
    private var surfaceTexture: SurfaceTexture? = null
    private var textureArray: IntArray = IntArray(1)
    private var width = 0
    private var height = 0
    private val effectApplied: Boolean
        get() = effect != Effect.NONE
    private val currentProgramId: Int
        get() = when (effect) {
            Effect.SEPIA -> sepiaProgramId
            Effect.NONE -> programId
            Effect.BLACK_AND_WHITE -> blackAndWhiteProgramId
        }

    init {
        initializeBuffers()
    }

    override fun onDrawFrame(gl: GL10?) {
        glClear(GL_COLOR_BUFFER_BIT)
        surfaceTexture?.updateTexImage()

        glUseProgram(currentProgramId)

        val textureLocation = glGetUniformLocation(programId, U_TEXT)
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_EXTERNAL_OES, textureArray[0])
        glUniform1i(textureLocation, 0)

        val positionLocation = glGetAttribLocation(programId, A_POSITION)
        verticesBuffer?.position(0)
        glVertexAttribPointer(positionLocation, 2, GL_FLOAT, false, 4 * 2, verticesBuffer)
        glEnableVertexAttribArray(positionLocation)

        glUniform1f(glGetUniformLocation(programId, U_EFFECT_APPLIED), if (effectApplied) 1f else 0f)

        val uvLocation = glGetAttribLocation(programId, A_UV)
        textureBuffer?.position(0)
        glVertexAttribPointer(uvLocation, 2, GL_FLOAT, false, 4 * 2, textureBuffer)
        glEnableVertexAttribArray(uvLocation)

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        glViewport(0, 0, width, height)
        this@CameraRenderer.width = width
        this@CameraRenderer.height = height
        camera?.parameters?.apply {
            val size = supportedPreviewSizes

            if (size.size > 0) {
                var i = 0

                while (i < size.size) {
                    if (size[i].width < width || size[i].height < height) break

                    i++
                }

                if (i > 0) i--

                setPreviewSize(size[i].width, size[i].height)
            }

            set("orientation", "landscape")
            camera?.parameters = this
            camera?.startPreview()
        }

    }

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        glClearColor(0f, 0.5f, 0.5f, 1f)
        initializeTextures()
        initializeCamera()
        programId = createProgram(CameraShader.fragmentShader)
        sepiaProgramId = createProgram(SepiaShader.fragmentShader)
        blackAndWhiteProgramId = createProgram(BlackAndWhiteShader.fragmentShader)
    }

    @SuppressLint("Recycle")
    private fun initializeCamera() {
        surfaceTexture = SurfaceTexture(textureArray[0])
        surfaceTexture?.setOnFrameAvailableListener {
            view?.requestRender()
        }
        camera = Camera.open()
        camera?.setPreviewTexture(surfaceTexture)
    }

    private fun initializeTextures() {
        glGenTextures(1, textureArray, 0)
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_EXTERNAL_OES, textureArray[0])
        glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    }

    private fun initializeBuffers() {
        verticesBuffer = ByteBuffer
                .allocateDirect(vertices.size * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()?.apply { put(vertices) }

        textureBuffer = ByteBuffer
                .allocateDirect(textureVertices.size * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()?.apply { put(textureVertices) }
    }

    private fun createProgram(fragmentShader: String): Int {
        val vertexShaderId = ShaderHelper.createShader(GL_VERTEX_SHADER, vertexShader)
        val fragmentShaderId = ShaderHelper.createShader(GL_FRAGMENT_SHADER, fragmentShader)

        return ShaderHelper.createProgram(vertexShaderId, fragmentShaderId)
    }

    private fun deleteTextures() = glDeleteTextures(1, textureArray, 0)

    fun stop() {
        camera?.stopPreview()
        camera?.release()
        surfaceTexture?.release()
        camera = null
        deleteTextures()
    }

}