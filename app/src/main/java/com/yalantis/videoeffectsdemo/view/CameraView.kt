package com.yalantis.videoeffectsdemo.view

import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet

/**
 * Created by irinagalata on 3/6/17.
 */
class CameraView : GLSurfaceView {

    var effect: Effect? = null
        set(value) {
            value?.let { renderer.effect = value }
        }

    val renderer = CameraRenderer()

    constructor(context: Context?) : super(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        setEGLContextClientVersion(2)
        setRenderer(renderer)
        renderer.view = this
        renderMode = RENDERMODE_WHEN_DIRTY
    }

    override fun onPause() {
        renderer.stop()
        super.onPause()
    }

}