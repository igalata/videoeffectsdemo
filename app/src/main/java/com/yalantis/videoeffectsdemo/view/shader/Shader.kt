package com.yalantis.videoeffectsdemo.view.shader

/**
 * Created by irinagalata on 3/10/17.
 */
interface Shader {

    val fragmentShader: String

}