package com.yalantis.videoeffectsdemo.flow.record

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import com.yalantis.videoeffectsdemo.R
import kotlinx.android.synthetic.main.fragment_record.*

/**
 * Created by irinagalata on 3/6/17.
 */
class RecordActivity : AppCompatActivity() {

    companion object {
        const val PERMISSIONS_REQUEST = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)
    }

    @SuppressLint("NewApi")
    override fun onResume() {
        super.onResume()
        if (permissionNeeded(this)) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), PERMISSIONS_REQUEST)
        } else {
            showCamera()
        }
    }

    private fun showCamera() = supportFragmentManager.beginTransaction()
            .replace(R.id.container, RecordFragment()).commit()


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST && grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showCamera()
        }
    }

    private fun permissionNeeded(context: Context): Boolean {
        return isM() && !permissionGranted(context, Manifest.permission.CAMERA)
    }

    private fun isM(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
    }

    private fun permissionGranted(context: Context, permission: String): Boolean {
        return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }

}